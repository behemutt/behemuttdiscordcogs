from redbot.core import commands
from discord.ext.commands import Bot
import discord

class SuggestionsCog(commands.Cog):
    """BEHEMUTT's suggestions and feedbacks cog"""

    def __init__(self, bot):
        self.bot = bot
        self._last_member = None

    @commands.command()
    async def suggestion(self, ctx, *, arg):
        """Get suggestion info and format in a fancy way"""
        embed=discord.Embed(title="Suggestion", description=arg, color=0x8000a9)
        embed.set_author(name=ctx.author, icon_url=ctx.author.avatar_url)
        authorid = '<@{}>'.format(ctx.author.id)
        channel = self.bot.get_channel(713475368593653840)
        
        await ctx.send("Thank you for your suggestion, {}!".format(authorid))
        sent = await channel.send(embed=embed)
        # await sent.add_reaction("<:bot:715994709746778133>")
        # await sent.add_reaction("<:botdead:715994748669788192>")
        await sent.add_reaction("⬆")
        await sent.add_reaction("⬇")