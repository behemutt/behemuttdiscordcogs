from .bugreportcog import BugReportCog

def setup(bot):
    bot.add_cog(BugReportCog(bot))