from redbot.core import commands
from discord.ext.commands import Bot
import discord

class BugReportCog(commands.Cog):
    """BEHEMUTT's bug reporting cog"""

    def __init__(self, bot):
        self.bot = bot
        self._last_member = None

    @commands.command()
    async def bug(self, ctx, *, arg):
        """Get bug info and format in a fancy way"""

        embed=discord.Embed(title="Bug Report", description=arg, color=0xff0000)
        embed.set_author(name=ctx.author, icon_url=ctx.author.avatar_url)
        authorid = '<@{}>'.format(ctx.author.id)
        channel = self.bot.get_channel(713475310892351649)
        await ctx.send("Thank you for reporting a bug, {}!".format(authorid))
        await channel.send(embed=embed)